package com.ml.service;

import java.util.Arrays;

import com.ml.model.DnaStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ml.model.Dna;
import com.ml.repository.SimianRepository;


@Service
public class SimianService {

    private DnaStatistics dnaStatistics;

    @Autowired
    private SimianRepository repository;

    public boolean checkSimian(String[] dna) {
    	dnaStatistics = new DnaStatistics();
        int dnaLength = dna.length;
        // Initialize and format to 2d array
        char[][] dnaSequence = createDna2DArray(dna, dnaLength);
        // Analise dna
        analiseGenes(dna, dnaLength, dnaSequence);
        // Save dna data
        saveResult(dna, dnaStatistics.getTotalSimiansGenes());
        // check if is simian
        return dnaStatistics.isSimian();
    }

    private char[][] createDna2DArray(String[] dna, int dnaLength) {
        // Check if dna size is valid (if every sequence has the same size)
        if (validSize(dna, dnaLength))
            throw new RuntimeException("Width and Height need to be equals (NxN array)");

        char[][] dnaSequence = new char[dnaLength][dnaLength];

        for (int row = 0; row < dnaLength; row++) {
            for (int column = 0; column < dnaLength; column++) {
                dnaSequence[row][column] = dna[row].charAt(column);
                //verify if only A/T/C/G exists at position
                if (!validGenes(dnaSequence[row][column]))
                    throw new RuntimeException("Must consist only of A/T/C/G letters");
            }
        }
        return dnaSequence;
    }

    private Boolean validSize(String[] dna, int dnaLength) {
        return Arrays.stream(dna).anyMatch(sequence -> sequence.length() != dnaLength);
    }

    private Boolean validGenes(char dnaSequence) {
        return (dnaSequence == 'A' || dnaSequence == 'T' || dnaSequence == 'G' || dnaSequence == 'C');
    }

    private void analiseGenes(String[] dna, int dnaLength, char[][] dnaSequence) {
        //Check all sides and add counter for matches
        checkHorizontalSequences(dna);
        checkVerticalSequence(dnaLength, dnaSequence);
        checkDiagonal(dnaLength, dnaSequence);
        checkInverseDiagonal(dnaLength, dnaSequence);
    }

    private void checkHorizontalSequences(String[] dna) {
        Arrays.stream(dna).forEach(sequence -> dnaStatistics.addSimiansGenes(sequence));
    }

    private void checkVerticalSequence(int dnaLength, char[][] dnaSequence) {
        StringBuilder sbVertical = new StringBuilder();
        for (int row = 0; row < dnaLength; row++) {
            for (int column = 0; column < dnaLength; column++) {
                sbVertical.append(dnaSequence[column][row]);
            }
            dnaStatistics.addSimiansGenes(sbVertical.toString());
            sbVertical = new StringBuilder();
        }
    }

    private void checkDiagonal(int dnaLength, char[][] dnaSequence) {
        //Check diagonals
        StringBuilder sbDiagonal = new StringBuilder();
        for (int diagonal = 0; diagonal < dnaLength; diagonal++) {
            //1st half (until biggest diagonal)
            for (int column = 0; column <= diagonal; column++) {
                int row = diagonal - column;
                sbDiagonal.append(dnaSequence[row][column]);
            }
            dnaStatistics.addSimiansGenes(sbDiagonal.toString());
            sbDiagonal = new StringBuilder();
        }

        //2nd half
        for (int row = dnaLength - 2; row >= 0; row--) {
            for (int column = 0; column <= row; column++) {
                int i = row - column;
                sbDiagonal.append(dnaSequence[dnaLength - column - 1][dnaLength - i - 1]);
            }
            dnaStatistics.addSimiansGenes(sbDiagonal.toString());
            sbDiagonal = new StringBuilder();
        }
    }

    private void checkInverseDiagonal(int dnaLength, char[][] dnaSequence) {
        //Check inverse diagonal
        StringBuilder sbInverseDiagonal = new StringBuilder();
        //1st half
        for (int row = dnaLength - 1; row > 0; row--) {
            for (int column = 0, x = row; x <= dnaLength - 1; column++, x++) {
                sbInverseDiagonal.append(dnaSequence[x][column]);
            }
            dnaStatistics.addSimiansGenes(sbInverseDiagonal.toString());
            sbInverseDiagonal = new StringBuilder();
        }
        //2nd half (starting on biggest diagonal)
        for (int row = 0; row <= dnaLength - 1; row++) {
            for (int column = 0, y = row; y <= dnaLength - 1; column++, y++) {
                sbInverseDiagonal.append(dnaSequence[column][y]);
            }
            dnaStatistics.addSimiansGenes(sbInverseDiagonal.toString());
            sbInverseDiagonal = new StringBuilder();
        }
    }

    private void saveResult(String[] dna, int countSimianGenes) {
    	String dnaFullSequence = Arrays.toString(dna);
    	//save only unique registry
    	if (repository.findByDnaFullSequence(dnaFullSequence) != null)
    		throw new RuntimeException("Has been inserted before / Non unique registry on DB");
        repository.save(new Dna(dnaFullSequence, countSimianGenes));
    }

}
