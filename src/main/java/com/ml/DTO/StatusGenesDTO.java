package com.ml.DTO;

public class StatusGenesDTO {

	private Long totalSimianDna;
	private Long totalHumanDna;
	private float ratio;

	public StatusGenesDTO(Long totalSimianDna, Long totalHumanDna, float ratio) {
		this.totalSimianDna = totalSimianDna;
		this.totalHumanDna = totalHumanDna;
		this.ratio = ratio;
	}

	public StatusGenesDTO() {}

	public Long getTotalSimianDna() {
		return totalSimianDna;
	}

	public Long getTotalHumanDna() {
		return totalHumanDna;
	}

	public float getRatio() {
		return ratio;
	}

	public void setTotalSimianDna(Long totalSimianDna) {
		this.totalSimianDna = totalSimianDna;
	}

	public void setTotalHumanDna(Long totalHumanDna) {
		this.totalHumanDna = totalHumanDna;
	}

	public void setRatio(float ratio) {
		this.ratio = ratio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(ratio);
		result = prime * result + ((totalHumanDna == null) ? 0 : totalHumanDna.hashCode());
		result = prime * result + ((totalSimianDna == null) ? 0 : totalSimianDna.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatusGenesDTO other = (StatusGenesDTO) obj;
		if (Float.floatToIntBits(ratio) != Float.floatToIntBits(other.ratio))
			return false;
		if (totalHumanDna == null) {
			if (other.totalHumanDna != null)
				return false;
		} else if (!totalHumanDna.equals(other.totalHumanDna))
			return false;
		if (totalSimianDna == null) {
			if (other.totalSimianDna != null)
				return false;
		} else if (!totalSimianDna.equals(other.totalSimianDna))
			return false;
		return true;
	}
	
	
}
