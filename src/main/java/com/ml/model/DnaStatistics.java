package com.ml.model;

import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

public class DnaStatistics {

    final List<String> genes = Arrays.asList("AAAA", "TTTT", "CCCC", "GGGG");

    private Integer totalSimiansGenes = 0;

    public void addSimiansGenes(String sequence) {
        Integer quantity = genes.stream()
                .map(gene -> StringUtils.countOccurrencesOf(sequence, gene))
                .reduce(0, Integer::sum);
        totalSimiansGenes += quantity;
    }

    public Integer getTotalSimiansGenes() {
        return totalSimiansGenes;
    }

    public Boolean isSimian() {
        return totalSimiansGenes >= 2;
    }
}
