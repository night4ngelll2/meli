package com.ml.model;

import javax.persistence.*;

@Entity
@Table(name = "dnas")
public class Dna {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "simianGenesSequences")
	private int simianGenesSequences;

		@Column(name = "dnaFullSequence")
	private String dnaFullSequence;
	
	
	public Dna (String dnaFullSequence, int simianGenesSequences) {
		this.dnaFullSequence = dnaFullSequence;
		this.simianGenesSequences = simianGenesSequences;
	}
	
	public int getSimianGenesSequences() {
		return simianGenesSequences;
	}

	public String getDnaFullSequence() {
		return dnaFullSequence;
	}
	
	
	public Dna() {

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dnaFullSequence == null) ? 0 : dnaFullSequence.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + simianGenesSequences;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dna other = (Dna) obj;
		if (dnaFullSequence == null) {
			if (other.dnaFullSequence != null)
				return false;
		} else if (!dnaFullSequence.equals(other.dnaFullSequence))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return simianGenesSequences == other.simianGenesSequences;
	}
	
	
}
