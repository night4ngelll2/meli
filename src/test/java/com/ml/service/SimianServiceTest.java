package com.ml.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.ml.model.Dna;
import com.ml.repository.SimianRepository;

@RunWith(MockitoJUnitRunner.class)
public class SimianServiceTest {

    @InjectMocks
    private SimianService service;

    @Mock
    private SimianRepository repository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void checkSimianTestFalse() {
        int expectedGeneCount = 0;
        String[] inputArray = {"AAACT",
                "TAATT",
                "ATCTT",
                "TCTAA",
                "ACCAA"};
        Dna dna = new Dna(Arrays.toString(inputArray), expectedGeneCount);
        Mockito.when(repository.save(dna)).thenReturn(dna);
        boolean checkSimian = service.checkSimian(inputArray);
        assertEquals(false, checkSimian);
    }

    @Test
    public void checkSimianTestTrue() {
        int expectedGeneCount = 4;
        String[] inputArray = {"AAAAT",
                "TAATT",
                "ATTTT",
                "TTTAA",
                "ACCTA"};
        Dna dna = new Dna(Arrays.toString(inputArray), expectedGeneCount);
        Mockito.when(repository.save(dna)).thenReturn(dna);
        assertEquals(true, service.checkSimian(inputArray));
    }

    @Test(expected = RuntimeException.class)
    public void invalidSizeInput() {
        String[] inputArray = {"AAAA",
                "TAAT",
                "ATTT",
                "TTTA",
                "ACCA"};
        service.checkSimian(inputArray);
    }


    @Test(expected = RuntimeException.class)
    public void invalidCharacterInput() {
        String[] inputArray = {"AABA",
                "TAAT",
                "ATTT",
                "TTTA",
                "ACCA"};
        service.checkSimian(inputArray);
    }

}
