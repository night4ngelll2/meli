package com.ml.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.ml.model.Dna;
import com.ml.DTO.StatusGenesDTO;
import com.ml.repository.SimianRepository;

public class StatusServiceTest {

	@InjectMocks
	private StatusService service;
	
	@Mock
	private SimianRepository repository;
	
	public List<Dna> dnaList = new ArrayList<Dna>();
	public Dna dna2 = new Dna("[AAAA,BBBB,CCCC,EEEE,DDDD", 0);
	public Dna dna1 = new Dna("[AAAA,BBBB,CCCC,EEEE,DDDD", 1);
	public Dna dna3 = new Dna("[AAAA,BBBB,CCCC,EEEE,DDDD", 3);
	public Dna dna4 = new Dna("[AAAA,BBBB,CCCC,EEEE,DDDD", 4);
	public Dna dna5 = new Dna("[AAAA,BBBB,CCCC,EEEE,DDDD", 5);
	
	@Before
	public void setUpDnaList() {
		MockitoAnnotations.initMocks(this);
		dnaList.add(dna1);
		dnaList.add(dna2);
		dnaList.add(dna3);
		dnaList.add(dna4);
		dnaList.add(dna5);
	}
	
	@Test
	public void checkGenesStatusTest() {
		StatusGenesDTO expectedStatus = new StatusGenesDTO(3L, 2L, 1.5f);
		Mockito.when(repository.findAll())
					.thenReturn(dnaList);
		StatusGenesDTO checkGenesStatus = service.checkGenesStatus();
		assertEquals(expectedStatus, checkGenesStatus);
	}
}
